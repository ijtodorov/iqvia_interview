import celery
from models.contact import ContactModel
from models.email import EmailModel
import random
import string


def random_string(string_length=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_length))


@celery.task()
def add_new_entry():
    new_contact = ContactModel(random_string(),random_string(),random_string())
    new_contact.save_to_database()
    first_mail = EmailModel(random_string(7)+"@gmail.com",new_contact.id)
    second_mail = EmailModel(random_string(7)+"@gmail.com",new_contact.id)
    first_mail.save_to_database()
    second_mail.save_to_database()
