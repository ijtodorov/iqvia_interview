from flask import Flask
from flask_restful import Api
from resources.contact import ContactManager, ContactList, ContactGet
from database import db
from models import email
from celery import Celery
import config
import celeryconfig

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

api = Api(app)

api.add_resource(ContactGet, '/contact/<string:username>')
api.add_resource(ContactManager, '/contact')
api.add_resource(ContactList, '/contacts')

db.init_app(app)

def make_celery(app):
    # create context tasks in celery
    celery = Celery(
        app.import_name,
        broker=config.BROKER_URL
    )
    celery.conf.update(app.config)
    celery.config_from_object(celeryconfig)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask

    return celery

celery = make_celery(app)

@app.before_first_request
def create_tables():
    db.create_all()


if __name__ == '__main__':
    app.run(port=5000)
