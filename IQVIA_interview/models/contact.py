from database import db
from models.email import EmailModel

class ContactModel(db.Model):
    __tablename__ = 'contacts'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    surname = db.Column(db.String(80))
    first_name = db.Column(db.String(80))
    emails = db.relationship('EmailModel')

    def __init__(self,username, first_name, surname):
        self.username = username
        self.first_name = first_name
        self.surname = surname


    def json(self):
        """Return dict (json) representation of object attributes"""
        return{'username':self.username,
               'first_name':self.first_name,
               'surname':self.surname,
               'email':[email.json() for email in self.emails]}

    def save_to_database(self):
        """Insert object to database"""
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    def delete_from_database(self):
        """Delete object from database"""
        db.session.delete(self)
        db.session.commit()


