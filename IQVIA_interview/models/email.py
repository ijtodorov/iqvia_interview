from database import db


class EmailModel(db.Model):
    __tablename__ = 'emails'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80))
    user_id = db.Column(db.Integer, db.ForeignKey('contacts.id'))
    username = db.relationship('ContactModel')

    def __init__(self, email, user_id):
        self.email = email
        self.user_id = user_id

    def json(self):
        return {"email" : self.email}

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email).first()

    def save_to_database(self):
        """Insert object to database"""
        db.session.add(self)
        db.session.commit()

    def delete_from_database(self):
        """Delete object from database"""
        db.session.delete(self)
        db.session.commit()


