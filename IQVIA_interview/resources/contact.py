from flask_restful import Resource, reqparse
from models.contact import ContactModel
from models.email import EmailModel

class Contacts(Resource):
    def get(self):
        pass


class ContactGet(Resource):

    def get(self, username):
        """Returns json for /contact/<username> if exists or 404 if missing"""
        item = ContactModel.find_by_username(username)
        if item:
            return item.json(),200
        mail = EmailModel.find_by_email(username)
        if mail:
            item = mail.username
            return item.json(),200
        return {'message': 'Item not found'}, 404


class ContactManager(Resource):
    """Resource implementing post, delete and put"""

    parser = reqparse.RequestParser()
    parser.add_argument('username',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!"
                        )
    parser.add_argument('first_name',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!"
                        )
    parser.add_argument('surname',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!"
                        )
    parser.add_argument('email',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!"
                        )

    def post(self):
        """Save contact to database Error when contact exists"""
        data = ContactManager.parser.parse_args()
        if ContactModel.find_by_username(username=data['username']):
            return {'message': "An item with username '{}' already exists.".format(data['username'])}, 400
        new_contact = ContactModel(data['username'],data['first_name'],data['surname'])
        new_contact.save_to_database()
        if EmailModel.find_by_email(data['email']):
            return {'message':'user with that email exists'}
        new_mail = EmailModel(data['email'],new_contact.id)
        new_mail.save_to_database()

        return new_contact.json(), 201

    def delete(self):
        """Delete item from database"""
        data = ContactManager.parser.parse_args()
        contact = ContactModel.find_by_username(data['username'])
        if contact:
            for mail in contact.emails:
                mail.delete_from_database()
            contact.delete_from_database()
            return {'message': 'Item deleted.'},200
        return {'message': 'Item not found.'}, 404

    def put(self):
        """Update or add item to database"""
        data = ContactManager.parser.parse_args()

        contact = ContactModel.find_by_username(data['username'])

        if contact:
            if EmailModel.find_by_email(data['email']):
                return {'message':'email is already in use'}, 400
            new_mail = EmailModel(data['email'],contact.id)
            new_mail.save_to_database()
            return contact.json(), 201
        else:
            new_contact = ContactModel(data['username'],data['first_name'],data['surname'])
            new_contact.save_to_database()
            if EmailModel.find_by_email(data['email']):
                return {'message':'user with that email exists'}, 400
            new_mail = EmailModel(data['email'],new_contact.id)
            new_mail.save_to_database()

        return new_contact.json(), 201


class ContactList(Resource):

    def get(self):
        """Get a list of all contacts"""
        return {'items': list(map(lambda x: x.json(), ContactModel.query.all()))}
