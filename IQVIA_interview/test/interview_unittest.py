import unittest
import app


class TestContact(unittest.TestCase):
    correct_dict = {
    "first_name": "Georgi",
    "username": "goshko",
    "surname": "Georgiev",
    "email": "goshko@abv.bg"
}
    expected_dict = {
    "email": [
        {
            "email": "goshko@abv.bg"
        }
    ],
    "username": "goshko",
    "first_name": "Georgi",
    "surname": "Georgiev"
}
    expected_dict_2 = {
    "email": [
        {
            "email": "goshko@abv.bg"
        },
        {
            "email": "goshko2@abv.bg"
        }
    ],
    "username": "goshko",
    "first_name": "Georgi",
    "surname": "Georgiev"
}
    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()
        #Create in memory database for testing
        app.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        with app.app.app_context():
            app.create_tables()

    def tearDown(self):
        """Reset database to have no hidden test dependancies"""
        #empty database, all tests should run in isolation
        app.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'
        with app.app.app_context():
            app.create_tables()

    def test_empty_db(self):
        """Database must be empty be default"""
        return_value = self.app.get('/contacts')
        self.assertEqual([],return_value.json['items'])

    def test_post_user_good_weather(self):
        """Test post user good weather"""
        return_value = self.app.post('/contact', data=self.correct_dict)
        self.assertEqual(return_value.status_code, 201, "Wrong Return code")
        self.assertEqual(return_value.json, self.expected_dict)

    def test_post_user_good_weather_multiple_mails_bad_weather(self):
        """Test post shouldn't be able to update users"""
        self.app.post('/contact', data=self.correct_dict)
        return_value = self.app.post('/contact', data=self.correct_dict)
        self.assertEqual(return_value.status_code, 400, "Wrong Return code")

    def test_post_user_user_exists(self):
        """Test post user bad weather"""
        self.app.post('/contact', data=self.correct_dict)
        return_value = self.app.post('/contact', data=self.correct_dict)
        self.assertEqual(return_value.status_code, 400, "Wrong Return code")
        self.assertEqual(return_value.json, {'message': "An item with username 'goshko' already exists."})

    def test_put_user_good_weather(self):
        """Test put should create a user if there is none with that username"""
        return_value = self.app.put('/contact', data=self.correct_dict)
        self.assertEqual(return_value.status_code, 201, "Wrong Return code")
        self.assertEqual(return_value.json, self.expected_dict)

    def test_put_user_good_weather_second_mail(self):
        """Test put should create a user if there is none with that username"""
        temp_dict = self.correct_dict.copy()
        temp_dict['email']='goshko2@abv.bg'
        self.app.post('/contact', data=self.correct_dict)
        return_value = self.app.put('/contact', data=temp_dict)
        self.assertEqual(return_value.status_code, 201, "Wrong Return code")
        self.assertEqual(return_value.json,self.expected_dict_2)

    def test_get_user_user_exists(self):
        """Test get user by username"""
        self.app.post('/contact', data=self.correct_dict)
        return_value = self.app.get('/contact/goshko')
        self.assertEqual(return_value.status_code, 200, "Wrong Return code")
        self.assertEqual(return_value.json, self.expected_dict)

    def test_get_user_by_email(self):
        """Test get user by username"""
        self.app.post('/contact', data=self.correct_dict)
        return_value = self.app.get('/contact/goshko@abv.bg')
        self.assertEqual(return_value.status_code, 200, "Wrong Return code")
        self.assertEqual(return_value.json, self.expected_dict)

    def test_delete_unexisting_user(self):
        """Test delete unexisting user"""
        modified_user = self.correct_dict.copy()
        modified_user['username'] = 'toshko'
        return_value = self.app.delete('/contact', data=modified_user)
        self.assertEqual(return_value.json, {'message': 'Item not found.'})
        self.assertEqual(return_value.status_code, 404, "Wrong Return code")

    def test_delete_user(self):
        """Test delete unexisting user"""
        self.app.post('/contact', data=self.correct_dict)
        return_value = self.app.delete('/contact', data=self.correct_dict)
        self.assertEqual(return_value.status_code, 200, "Wrong Return code")
        self.assertEqual(return_value.json, {'message': 'Item deleted.'})


if __name__ == "__main__":
    unittest.main()

